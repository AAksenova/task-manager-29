package ru.t1.aksenova.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.aksenova.tm.api.repository.ITaskRepository;
import ru.t1.aksenova.tm.model.Task;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @NotNull
    @Override
    public Task create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) {
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        add(userId, task);
        return task;
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(
            @NotNull final String userId,
            @NotNull final String projectId
    ) {
        @NotNull final List<Task> findTasks = new ArrayList<>();
        for (final Task task : records) {
            if (task.getProjectId() == null) continue;
            if (projectId.equals(task.getProjectId())) findTasks.add(task);
        }
        if (findTasks.size() <= 0) return Collections.emptyList();
        return findTasks;
    }

}
