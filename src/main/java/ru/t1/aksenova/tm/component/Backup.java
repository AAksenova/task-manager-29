package ru.t1.aksenova.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.aksenova.tm.command.data.AbstractDataCommand;
import ru.t1.aksenova.tm.command.data.DataBackupLoadCommand;
import ru.t1.aksenova.tm.command.data.DataBackupSaveCommand;

import java.nio.file.Files;
import java.nio.file.Paths;

public final class Backup extends Thread {

    @NotNull
    private final Bootstrap bootstrap;

    public Backup(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        this.setDaemon(true);
    }

    @Override
    @SneakyThrows
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            Thread.sleep(3000);
            bootstrap.processCommand(DataBackupSaveCommand.NAME, false);
        }
    }

    public void init() {
        load();
        start();
    }

    public void load() {
        @NotNull final String fileName = AbstractDataCommand.FILE_BACKUP;
        if (!Files.exists(Paths.get(fileName))) return;
        bootstrap.processCommand(DataBackupLoadCommand.NAME, false);
    }

}
