package ru.t1.aksenova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.api.service.IUserService;
import ru.t1.aksenova.tm.enumerated.Role;
import ru.t1.aksenova.tm.util.TerminalUtil;

public final class UserUpdateProfileCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "update-user-profile";

    @NotNull
    public static final String DESCRIPTION = "Update profile of current user.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        @Nullable final String userId = getUserId();
        System.out.println("[USER UPDATE PROFILE]");
        System.out.println("ENTER FIRST NAME:");
        @Nullable final String firstName = TerminalUtil.nextLine();
        System.out.println("ENTER LAST NAME:");
        @Nullable final String lastName = TerminalUtil.nextLine();
        System.out.println("ENTER MIDDLE NAME:");
        @Nullable final String middleName = TerminalUtil.nextLine();
        @NotNull final IUserService userService = serviceLocator.getUserService();
        userService.updateUser(userId, firstName, lastName, middleName);
    }

    @Nullable
    public Role[] getRoles() {
        return Role.values();
    }

}
